package com.example.guilh.pmod05_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Finalizacao extends AppCompatActivity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalizacao);

        Intent intencao = getIntent();
        Bundle parametros = intencao.getExtras();

        Button botao = (Button)findViewById(R.id.btRecomecar);
        botao.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intencao = new Intent(this, TelaPrincipal.class);
        startActivity(intencao);
    }
}
