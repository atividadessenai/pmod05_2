package com.example.guilh.pmod05_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

public class TelaAvaliacao extends AppCompatActivity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_avaliacao);

        Intent intencao = getIntent();
        Bundle parametros = intencao.getExtras();

        String parametroNome = parametros.getString("nome");
        String parametroEmail = parametros.getString("email");
        String parametroTelefone = parametros.getString("telefone");

        TextView nome = (TextView)findViewById(R.id.tvNomeAvaliacao);
        TextView email = (TextView)findViewById(R.id.tvEmailAvaliacao);
        TextView telefone = (TextView)findViewById(R.id.tvTelefoneAvaliacao);

        nome.setText(parametroNome);
        email.setText(parametroEmail);
        telefone.setText(parametroTelefone);

        Button botao = (Button)findViewById(R.id.btConcluir);
        botao.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intencao = new Intent(this, Finalizacao.class);
        TextView nome = (TextView)findViewById(R.id.tvNomeAvaliacao);
        TextView email = (TextView)findViewById(R.id.tvEmailAvaliacao);
        TextView telefone = (TextView)findViewById(R.id.tvTelefoneAvaliacao);
        RatingBar avaliacao = (RatingBar)findViewById(R.id.rtAvaliacao);

        Bundle parametros = new Bundle();
        parametros.putString("nome", nome.getText().toString());
        parametros.putString("email", email.getText().toString());
        parametros.putString("telefone", telefone.getText().toString());
        parametros.putString("avaliacao", Float.toString(avaliacao.getRating()));

        intencao.putExtras(parametros);

        startActivity(intencao);
    }
}
