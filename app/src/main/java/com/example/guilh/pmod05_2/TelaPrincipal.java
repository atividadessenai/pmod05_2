package com.example.guilh.pmod05_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaPrincipal extends AppCompatActivity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        Button botao = (Button)findViewById(R.id.btIniciar);
        botao.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intencao = new Intent(this, TelaAvaliacao.class);

        EditText nome = (EditText)findViewById(R.id.etNome);
        EditText email = (EditText)findViewById(R.id.etEmail);
        EditText telefone = (EditText)findViewById(R.id.etTelefone);

        Bundle parametros = new Bundle();
        parametros.putString("nome", nome.getText().toString());
        parametros.putString("email", email.getText().toString());
        parametros.putString("telefone", telefone.getText().toString());

        intencao.putExtras(parametros);

        startActivity(intencao);
    }
}
